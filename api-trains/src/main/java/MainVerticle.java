import api_trains.handlersGET.TrainsApiGET;
import io.vertx.core.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;

import java.util.HashSet;
import java.util.Set;

public class MainVerticle extends AbstractVerticle {

  private final static int PORT = 8080;

  @Override
  public void start(final Promise<Void> startPromise) {
    //creating Vertx router and providing handlers
    Router router = Router.router(vertx);
    router.route().handler(BodyHandler.create());
    router.route().handler(getCorsPolicyHandler());
    router.route().handler(StaticHandler.create().setCachingEnabled(false));

    //instantiating GET handlers utilities from our API
    TrainsApiGET handlersGET = TrainsApiGET.createInstance(vertx);

    //adding to router GET routes and linking handlers from our API
    router.get("/train-solution").handler(handlersGET::getTrainSolutions); //ctx -> handlersGET.getTrainSolutions(ctx)
    router.get("/real-time-train-info").handler(handlersGET::getRealTimeTrainInfo);
    router.get("/real-time-station-info").handler(handlersGET::getRealTimeStationInfo);

    //starting httpServer
    vertx.createHttpServer().requestHandler(router).listen(PORT);
    System.out.println("SERVER LISTENING ON PORT " + PORT);
  }

  /**
    Cors Policy allowing client to contact server
   */
  private CorsHandler getCorsPolicyHandler() {
    Set<String> allowedHeaders = new HashSet<>();
    allowedHeaders.add("Access-Control-Allow-Origin");
    allowedHeaders.add("origin");
    allowedHeaders.add("Content-Type");
    allowedHeaders.add("accept");
    Set<HttpMethod> allowedMethods = new HashSet<>();
    allowedMethods.add(HttpMethod.GET);
    allowedMethods.add(HttpMethod.POST);
    allowedMethods.add(HttpMethod.OPTIONS);
    return CorsHandler.create(".*.").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods);
  }

}
