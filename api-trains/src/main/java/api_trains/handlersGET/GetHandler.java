package api_trains.handlersGET;

import com.google.gson.Gson;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;

/**
 * Common behaviour for GET handlers.
 * The handle() method handles the GET http request asynchronously.
 * @param <A> defines the Class used in the promise result
 */
public class GetHandler<A> {

  private final Vertx vertx;
  private final RoutingContext routingContext;
  private final Promise<A> promise;
  private final HttpRequestStrategy httpRequestStrategy;
  private final CompletedPromiseValueStrategy<A> completedPromiseValueStrategy;

  public GetHandler(
    final Vertx vertx,
    final RoutingContext routingContext,
    final HttpRequestStrategy httpRequestStrategy,
    final CompletedPromiseValueStrategy<A> completedPromiseValueStrategy
  ){
    this.vertx = vertx;
    this.routingContext = routingContext;
    this.promise = Promise.promise();
    this.httpRequestStrategy = httpRequestStrategy;
    this.completedPromiseValueStrategy = completedPromiseValueStrategy;
  }

  public void handle(){
    try{
      contactServer();
      onCompletion();
    } catch (Exception e){
      routingContext.response().setStatusCode(400).end();
    }
  }

  private void contactServer(){
    //puts the handler in the event queue of Vertx
    vertx.runOnContext(r -> {
      try {
        HttpResponse<String> response =
          HttpClient
            .newHttpClient()
            .send(
              httpRequestStrategy.apply(), //the actual http request will be provided by strategy
              HttpResponse.BodyHandlers.ofString()
            );
        if(response.statusCode() != 200){
          promise.fail(String.valueOf(response.statusCode()));
        } else {
          promise.complete(
            completedPromiseValueStrategy.apply(response.body()) //promise completion will be provided by strategy
          );
        }
      } catch (Exception e) {
        promise.fail("500");
      }
    });
  }

  private void onCompletion(){
    /*
      on success: sends to client a JSON containing the result
      on failure sends to client the error code provided by contacted server
    */
    promise.future()
      .onSuccess((A result) -> routingContext.response().putHeader("content-type", "application/json").end(new Gson().toJson(result)))
      .onFailure((Throwable t) -> routingContext.response().setStatusCode(Integer.parseInt(t.getMessage())).end());
  }

}
