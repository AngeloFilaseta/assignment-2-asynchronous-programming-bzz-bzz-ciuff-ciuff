package api_trains.handlersGET;

import java.net.http.HttpRequest;

/**
 Generic inteface defining the strategy to use when a GET handler contacts an external server.
 */
public interface HttpRequestStrategy {

  HttpRequest apply();

}
