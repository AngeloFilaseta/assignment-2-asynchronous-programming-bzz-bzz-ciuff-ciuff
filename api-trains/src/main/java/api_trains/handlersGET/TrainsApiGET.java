package api_trains.handlersGET;

import api_trains.handlersGET.realTimeStationInfo.RealTimeStationInfoGetHandler;
import api_trains.handlersGET.realTimeTrainInfo.RealTimeTrainInfoGetHandler;
import api_trains.handlersGET.trainSolution.TrainSolutionGetHandler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

/**
 * Utility class exposing GET methods in trains API.
 * Uses an instance of Vertx to be instantiated and a RoutingContext instance to invoke a handler
 * (Vertx instance never changes, RoutingContext changes based on incoming server traffic).
 */
public class TrainsApiGET {

  private final Vertx vertx;

  public static TrainsApiGET createInstance(final Vertx vertx){
    return new TrainsApiGET(vertx);
  }

  private TrainsApiGET(final Vertx vertx){
    this.vertx = vertx;
  }

  public void getRealTimeStationInfo(final RoutingContext ctx){
    RealTimeStationInfoGetHandler.createInstance(vertx, ctx).handle();
  }

  public void getRealTimeTrainInfo(final RoutingContext ctx){
    RealTimeTrainInfoGetHandler.createInstance(vertx, ctx).handle();
  }

  public void getTrainSolutions(final RoutingContext ctx){
    TrainSolutionGetHandler.createInstance(vertx, ctx).handle();
  }

}
