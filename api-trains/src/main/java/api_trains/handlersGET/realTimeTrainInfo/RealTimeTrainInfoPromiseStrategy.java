package api_trains.handlersGET.realTimeTrainInfo;

import api_trains.handlersGET.CompletedPromiseValueStrategy;
import api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder.TrainInfoDecoder;
import api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder.TrainInfoDecoderFactory;

public class RealTimeTrainInfoPromiseStrategy implements CompletedPromiseValueStrategy<TrainInfoDecoder> {

  @Override
  public TrainInfoDecoder apply(String response) {
    return TrainInfoDecoderFactory.getTrainInfoDecoder(response);
  }

}
