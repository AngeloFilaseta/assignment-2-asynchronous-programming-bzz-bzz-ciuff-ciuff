package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.Optional;

public class TrainInfoDecoderImpl implements TrainInfoDecoder{

    private final String name;
    private final TrainStopWithRailDecoder departure;
    private final TrainStopWithRailDecoder arrival;
    private final TrainStopDecoder lastStop;

    protected TrainInfoDecoderImpl(final String html) {
      Document doc = Jsoup.parse(html);
      name = doc.getElementsByTag("h1").text().trim();
      Elements trainStops = doc.getElementsByClass("corpocentrale");
      departure = new TrainStopWithRailDecoderImpl(trainStops.get(0));
      arrival = (trainStops.size() == 3) ? new TrainStopWithRailDecoderImpl(trainStops.get(2)) : new TrainStopWithRailDecoderImpl(trainStops.get(1));
      lastStop = (trainStops.size() == 3) ? new TrainStopDecoderImpl(trainStops.get(1)) : null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public TrainStopWithRailDecoder getDepartureDecoder() {
        return departure;
    }

    @Override
    public TrainStopWithRailDecoder getArrivalDecoder() {
        return arrival;
    }

    @Override
    public Optional<TrainStopDecoder> getLastStop() {
        return lastStop == null ? Optional.empty() : Optional.of(lastStop);
    }

}
