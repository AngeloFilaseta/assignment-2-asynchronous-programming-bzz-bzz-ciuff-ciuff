package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import java.util.Optional;

public interface TrainStopWithRailDecoder extends TrainStopDecoder{

    String getScheduledRail();

    Optional<String> getActualRail();

}
