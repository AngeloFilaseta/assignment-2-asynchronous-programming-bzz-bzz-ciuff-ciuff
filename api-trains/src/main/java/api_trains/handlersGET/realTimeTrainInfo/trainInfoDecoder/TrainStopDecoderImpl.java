package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Optional;

public class TrainStopDecoderImpl implements TrainStopDecoder{

    private final String station;
    private final String scheduledTime;
    private final String actualTime;

    protected TrainStopDecoderImpl(final Element elem){
        station = elem.getElementsByTag("h2").text().trim();
        Elements times = elem.getElementsByTag("strong");
        scheduledTime = times.get(0).text().trim();
        actualTime = times.get(1).text().trim();
    }

    @Override
    public String getStation() {
        return station;
    }

    @Override
    public String getScheduledTime() {
        return scheduledTime;
    }

    @Override
    public Optional<String> getActualTime() {
        return Optional.of(actualTime);
    }

}
