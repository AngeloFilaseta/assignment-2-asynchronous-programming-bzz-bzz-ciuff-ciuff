package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

public class TrainInfoDecoderFactory {

    private TrainInfoDecoderFactory(){

    }

    public static TrainInfoDecoder getTrainInfoDecoder(final String html) {
        return new TrainInfoDecoderImpl(html);
    }
}
