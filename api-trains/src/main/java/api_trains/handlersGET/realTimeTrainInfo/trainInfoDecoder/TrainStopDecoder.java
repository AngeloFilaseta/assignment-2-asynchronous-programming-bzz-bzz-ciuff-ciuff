package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import java.util.Optional;

public interface TrainStopDecoder {

    String getStation();

    String getScheduledTime();

    Optional<String> getActualTime();

}
