package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import java.util.Optional;

public interface TrainInfoDecoder {

    String getName();

    TrainStopWithRailDecoder getDepartureDecoder();

    TrainStopWithRailDecoder getArrivalDecoder();

    Optional<TrainStopDecoder> getLastStop();

}
