package api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Optional;

public class TrainStopWithRailDecoderImpl implements TrainStopWithRailDecoder{

    private final String station;
    private final String scheduledTime;
    private final String actualTime;
    private final String scheduledRail;
    private final String actualRail;

    protected TrainStopWithRailDecoderImpl(final Element elem){
        station = elem.getElementsByTag("h2").text().trim();
        Elements times = elem.getElementsByTag("p");
        scheduledTime = times.get(0).getElementsByTag("strong").get(0).text().trim();
        String actualTimeTmp = times.get(1).getElementsByTag("strong").get(0).text().trim();
        actualTime = (actualTimeTmp.isEmpty()) ?  null : actualTimeTmp;
        scheduledRail = elem.toString().split("Binario Previsto:")[1].split("<br>")[1].trim();
        String actualRailTmp = elem.toString().split("Binario Reale:")[1].split("<br>")[1].split("</div>")[0].trim();
        actualRail = actualRailTmp.contains("--") ? null : actualRailTmp.split("<strong>")[1].split("</strong>")[0].trim();
    }

    @Override
    public String getStation(){
        return station;
    }

    @Override
    public String getScheduledTime(){
        return scheduledTime;
    }

    @Override
    public Optional<String> getActualTime(){
        return actualTime == null ? Optional.empty() : Optional.of(actualTime);
    }

    @Override
    public String getScheduledRail(){
        return scheduledRail;
    }

    @Override
    public Optional<String> getActualRail(){
        return actualRail == null ? Optional.empty() : Optional.of(actualRail);
    }

}
