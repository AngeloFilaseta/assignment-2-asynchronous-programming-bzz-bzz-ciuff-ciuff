package api_trains.handlersGET.realTimeTrainInfo;

import api_trains.handlersGET.HttpRequestStrategy;
import io.vertx.ext.web.RoutingContext;

import java.net.URI;
import java.net.http.HttpRequest;

public class RealTimeTrainInfoHttpStrategy implements HttpRequestStrategy {

  private final RoutingContext routingContext;

  protected RealTimeTrainInfoHttpStrategy(final RoutingContext routingContext){
    this.routingContext = routingContext;
  }

  @Override
  public HttpRequest apply() {
    return HttpRequest
      .newBuilder()
      .GET()
      .uri(createURI())
      .build();
  }

  private URI createURI() throws NullPointerException, NumberFormatException{
    return URI.create(
      "http://www.viaggiatreno.it/vt_pax_internet/mobile/scheda?numeroTreno=" +
        Integer.parseInt(routingContext.request().getParam("trainNumber").trim())
    );
  }

}
