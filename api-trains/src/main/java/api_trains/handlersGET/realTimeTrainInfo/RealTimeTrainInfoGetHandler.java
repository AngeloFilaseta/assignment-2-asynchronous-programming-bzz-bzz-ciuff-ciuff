package api_trains.handlersGET.realTimeTrainInfo;

import api_trains.handlersGET.GetHandler;
import api_trains.handlersGET.realTimeTrainInfo.trainInfoDecoder.TrainInfoDecoder;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

public class RealTimeTrainInfoGetHandler extends GetHandler<TrainInfoDecoder> {

  public static RealTimeTrainInfoGetHandler createInstance(final Vertx vertx, final RoutingContext routingContext){
    return new RealTimeTrainInfoGetHandler(vertx, routingContext);
  }

  private RealTimeTrainInfoGetHandler(final Vertx vertx, final RoutingContext routingContext){
    super(
      vertx,
      routingContext,
      new RealTimeTrainInfoHttpStrategy(routingContext),
      new RealTimeTrainInfoPromiseStrategy()
    );
  }

}
