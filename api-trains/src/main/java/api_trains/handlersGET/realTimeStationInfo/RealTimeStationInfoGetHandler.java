package api_trains.handlersGET.realTimeStationInfo;

import api_trains.handlersGET.GetHandler;
import api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder.StationInfoDecoder;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

public class RealTimeStationInfoGetHandler extends GetHandler<StationInfoDecoder> {

  public static RealTimeStationInfoGetHandler createInstance(final Vertx vertx, final RoutingContext routingContext){
    return new RealTimeStationInfoGetHandler(vertx, routingContext);
  }

  private RealTimeStationInfoGetHandler(final Vertx vertx, final RoutingContext routingContext) {
    super(
      vertx,
      routingContext,
      new RealTimeStationInfoHttpStrategy(routingContext),
      new RealTimeStationInfoPromiseStrategy());
  }

}
