package api_trains.handlersGET.realTimeStationInfo;

import api_trains.handlersGET.CompletedPromiseValueStrategy;
import api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder.StationInfoDecoder;
import api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder.StationInfoDecoderFactory;

public class RealTimeStationInfoPromiseStrategy implements CompletedPromiseValueStrategy<StationInfoDecoder> {

  @Override
  public StationInfoDecoder apply(final String response) {
    return StationInfoDecoderFactory.getStationInfoDecoder(response);
  }

}
