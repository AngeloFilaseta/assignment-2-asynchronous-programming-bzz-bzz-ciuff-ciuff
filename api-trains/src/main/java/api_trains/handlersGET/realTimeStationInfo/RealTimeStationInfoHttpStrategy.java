package api_trains.handlersGET.realTimeStationInfo;

import api_trains.handlersGET.HttpRequestStrategy;
import io.vertx.ext.web.RoutingContext;

import java.net.URI;
import java.net.http.HttpRequest;

public class RealTimeStationInfoHttpStrategy implements HttpRequestStrategy {

  private final RoutingContext routingContext;

  protected RealTimeStationInfoHttpStrategy(final RoutingContext routingContext){
    this.routingContext = routingContext;
  }

  @Override
  public HttpRequest apply() {
    return HttpRequest
      .newBuilder()
      .POST(HttpRequest.BodyPublishers.ofString(""))
      .uri(createURI())
      .build();
  }

  private URI createURI(){
    return URI.create(
      "http://www.viaggiatreno.it/vt_pax_internet/mobile/stazione?codiceStazione=" +
        routingContext.request().getParam("stationCode").trim()
    );
  }

}
