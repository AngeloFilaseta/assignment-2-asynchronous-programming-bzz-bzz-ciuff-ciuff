package api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashSet;
import java.util.Set;

public class StationInfoDecoderImpl implements StationInfoDecoder{

    private final Set<TrainDecoder> departures;
    private final Set<TrainDecoder> arrivals;

    protected StationInfoDecoderImpl(final String html) {
        Document doc = Jsoup.parse(html);
        departures = new HashSet<>();
        arrivals = new HashSet<>();
        for(Element element : doc.getElementsByClass("bloccorisultato")){
            if (element.toString().contains("Per <strong>")) {
                departures.add(new TrainDecoderImpl(element));
            } else {
                arrivals.add(new TrainDecoderImpl(element));
            }
        }
    }

    @Override
    public Set<TrainDecoder> getDepartures() {
        return departures;
    }

    @Override
    public Set<TrainDecoder> getArrivals() {
        return arrivals;
    }

}
