package api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder;

import java.util.Optional;

public interface TrainDecoder {

    String getStation();

    String getName();

    String getTime();

    String getScheduledRail();

    Optional<String> getRealRail();

}
