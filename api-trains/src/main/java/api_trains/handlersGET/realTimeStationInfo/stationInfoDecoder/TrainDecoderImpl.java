package api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder;

import org.jsoup.nodes.Element;

import java.util.Optional;

public class TrainDecoderImpl implements TrainDecoder{

    private final String station;
    private final String name;
    private final String time;
    private final String scheduledRail;
    private final String realRail;

    protected TrainDecoderImpl(final Element element){
        station = element.getElementsByTag("strong").get(0).text().trim();
        name = element.getElementsByTag("h2").get(0).text().trim();
        time = element.getElementsByTag("strong").get(1).text().trim();
        scheduledRail = element.toString().split("Binario Previsto:")[1].split("<br>")[0].trim();
        String realRailTmp = element.toString().split("Binario Reale:")[1].split("<br>")[0].trim();
        realRail = realRailTmp.equals("--") ? null : realRailTmp.split("<strong>")[1].split("</strong>")[0].trim();
    }

    @Override
    public String getStation() {
        return station;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public String getScheduledRail() {
        return scheduledRail;
    }

    @Override
    public Optional<String> getRealRail() {
        return realRail == null ? Optional.empty() : Optional.of(realRail);
    }

    @Override
    public String toString(){
        return "origin/destination station:\t" + station + "\n" +
                "train name:\t" + name + "\n" +
                "train time:\t" + time + "\n" +
                "scheduled rail:\t" + scheduledRail + "\n" +
                "real rail:\t" + getRealRail();
    }
}
