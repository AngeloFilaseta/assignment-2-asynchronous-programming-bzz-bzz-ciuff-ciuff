package api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder;

public class StationInfoDecoderFactory {

    private StationInfoDecoderFactory(){

    }

    public static StationInfoDecoder getStationInfoDecoder(final String html) {
        return new StationInfoDecoderImpl(html);
    }


}
