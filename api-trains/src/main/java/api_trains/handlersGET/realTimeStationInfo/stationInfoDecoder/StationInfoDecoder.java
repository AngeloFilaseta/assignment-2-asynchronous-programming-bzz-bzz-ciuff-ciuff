package api_trains.handlersGET.realTimeStationInfo.stationInfoDecoder;

import java.util.Set;

public interface StationInfoDecoder {

    Set<TrainDecoder> getDepartures();

    Set<TrainDecoder> getArrivals();

}
