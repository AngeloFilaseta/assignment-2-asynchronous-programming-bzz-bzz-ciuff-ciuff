package api_trains.handlersGET;

/**
  Generic inteface defining the strategy to use when a GET handler completes a promise.
 */
public interface CompletedPromiseValueStrategy<A> {

  A apply(final String response);

}
