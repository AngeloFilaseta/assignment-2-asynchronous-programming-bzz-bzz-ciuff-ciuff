package api_trains.handlersGET.trainSolution.trainSolutionDecoder.time;

import java.util.Calendar;
import java.util.Date;

public class TimeFactory {

    public static Time fromLong(long millis){
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(new Date(millis));
      return new TimeImpl(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));
    }

    public static Time fromString(String timeString){
      int hours = Integer.parseInt(timeString.substring(0,2));
      if(timeString.length() > 3){
        int minutes = Integer.parseInt(timeString.substring(3,5));
        if(timeString.length() > 6){
          int seconds = Integer.parseInt(timeString.substring(6,8));
          return createTime(hours, minutes, seconds);
        }else{
          return createTime(hours, minutes);
        }
      }
      return createTime(hours);
    }

    public static Time createTime(final int hours, final int minutes, final int seconds){
        return new TimeImpl(hours, minutes, seconds);
    }

    public static Time createTime(final int hours, final int minutes){
        return new TimeImpl(hours, minutes);
    }

    public static Time createTime(final int hours){
        return new TimeImpl(hours);
    }

}
