package api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution;

import api_trains.handlersGET.trainSolution.trainSolutionDecoder.time.Time;

import java.util.List;

public class SolutionImpl implements Solution {

  private final String id;
  private final List<String> listOfTrainsName;
  private final String departureStationName;
  private final String arrivalStationName;
  private final Time departureTime;
  private final Time arrivalTime;
  private final Time duration;
  private final double originalPrice;

  public SolutionImpl(final String id,
                      final List<String> listOfTrainsName,
                      final String departureStation,
                      final String arrivalStation,
                      final Time departureTime,
                      final Time arrivalTime,
                      final Time duration,
                      final double originalPrice) {
    this.id = id;
    this.listOfTrainsName = listOfTrainsName;
    this.departureStationName = departureStation;
    this.arrivalStationName = arrivalStation;
    this.departureTime = departureTime;
    this.arrivalTime = arrivalTime;
    this.duration = duration;
    this.originalPrice = originalPrice;
  }

  @Override
  public String getID() {
    return id;
  }

  @Override
  public List<String> getListOfTrainsName() {
    return listOfTrainsName;
  }

  @Override
  public String getDepartureStationName() {
    return departureStationName;
  }

  @Override
  public String getArrivalStationName() {
    return arrivalStationName;
  }

  @Override
  public Time getDepartureTime() {
    return departureTime;
  }

  @Override
  public Time getArrivalTime() {
    return arrivalTime;
  }

  @Override
  public Time getDuration() {
    return duration;
  }

  @Override
  public double getOriginalPrice() {
    return originalPrice;
  }
}
