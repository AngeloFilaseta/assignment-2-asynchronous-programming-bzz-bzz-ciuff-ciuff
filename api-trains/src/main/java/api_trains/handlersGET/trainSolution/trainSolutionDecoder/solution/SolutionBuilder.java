package api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution;

import api_trains.handlersGET.trainSolution.trainSolutionDecoder.time.Time;

import java.util.List;

public class SolutionBuilder {

  private String id;
  private List<String> listOfTrains;
  private String departureStation;
  private String arrivalStation;
  private Time departureTime;
  private Time arrivalTime;
  private Time duration;
  private double originalPrice = Long.MIN_VALUE;

    public SolutionBuilder setID(final String id){
      this.id = id;
      return this;
    }

    public SolutionBuilder setListOfTrains(final List<String> listOfTrains){
      this.listOfTrains = listOfTrains;
      return this;
    }

    public SolutionBuilder setDepartureStationName(final String departureStation){
      this.departureStation = departureStation;
      return this;
    }

    public SolutionBuilder setArrivalStationName(final String arrivalStation){
      this.arrivalStation = arrivalStation;
      return this;
    }

    public SolutionBuilder setDepartureTime(final Time departureTime){
      this.departureTime = departureTime;
      return this;
    }

    public SolutionBuilder setArrivalTime(final Time arrivalTime){
      this.arrivalTime = arrivalTime;
      return this;
    }

    public SolutionBuilder setDuration(final Time duration){
      this.duration = duration;
      return this;
    }

    public SolutionBuilder setOriginalPrice(final double originalPrice){
      this.originalPrice = originalPrice;
      return this;
    }

    public SolutionImpl build(){
      if(id == null || listOfTrains == null || departureStation == null || arrivalStation == null || departureTime == null || arrivalTime == null || duration == null || originalPrice == Long.MIN_VALUE){
        throw new IllegalStateException("A field has not been initialized correctly.");
      }
      return new SolutionImpl(id, listOfTrains, departureStation, arrivalStation, departureTime, arrivalTime, duration, originalPrice);
    }
}
