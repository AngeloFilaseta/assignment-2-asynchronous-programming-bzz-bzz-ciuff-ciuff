package api_trains.handlersGET.trainSolution.trainSolutionDecoder;

import io.vertx.core.http.HttpServerRequest;

public class TrainsSolutionParamsImpl implements TrainSolutionParams{

  private final String departure;
  private final String arrival;
  private final String date;
  private final String hour;

  public TrainsSolutionParamsImpl(final HttpServerRequest httpServerRequest) throws IllegalStateException {
    try{
      departure = httpServerRequest.getParam("departure").trim().toUpperCase().replace(" ", "%20");
      arrival = httpServerRequest.getParam("arrival").trim().toUpperCase().replace(" ", "%20");
      date = httpServerRequest.getParam("date").trim();
      hour = httpServerRequest.getParam("time").trim();
    } catch (Exception e){
      throw new IllegalStateException();
    }
  }


  @Override
  public String getDeparture() {
    return departure;
  }

  @Override
  public String getArrival() {
    return arrival;
  }

  @Override
  public String getDate() {
    return date;
  }

  @Override
  public String getHour() {
    return hour;
  }
}
