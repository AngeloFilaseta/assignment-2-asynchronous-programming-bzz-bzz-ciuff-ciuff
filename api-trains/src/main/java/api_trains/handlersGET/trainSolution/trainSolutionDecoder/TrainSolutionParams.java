package api_trains.handlersGET.trainSolution.trainSolutionDecoder;

public interface TrainSolutionParams {

  String getDeparture();

  String getArrival();

  String getDate();

  String getHour();

}
