package api_trains.handlersGET.trainSolution.trainSolutionDecoder.time;

public class TimeImpl implements Time {

  private final static int MAX_HOURS_VALUE = 23;
  private final static int MAX_MINUTES_VALUE = 59;
  private final static int MAX_SECONDS_VALUE = 59;

  private final int hours;
  private final int minutes;
  private final int seconds;

  public TimeImpl(final int hours, final int minutes, final int seconds) {
    checkTimeIsCorrect(hours, minutes, seconds);
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
  }

  public TimeImpl(final int hours, final int minutes) {
    this(hours, minutes, 0);
  }

  public TimeImpl(final int hours) {
    this(hours, 0, 0);
  }

  @Override
  public int getHours() {
    return hours;
  }

  @Override
  public int getMinutes() {
    return minutes;
  }

  @Override
  public int getSeconds() {
    return seconds;
  }

  private void checkTimeIsCorrect(final int hours, final int minutes, final int seconds){
    if((hours > MAX_HOURS_VALUE || hours < 0)
      && (minutes > MAX_MINUTES_VALUE || minutes < 0)
      && (seconds > MAX_SECONDS_VALUE || seconds < 0)){
      throw new IllegalArgumentException("Wrong time value(s) passed as parameters.");
    }
  }

  @Override
  public String toString() {
    return hours + ":" + minutes + ":" + seconds;
  }
}
