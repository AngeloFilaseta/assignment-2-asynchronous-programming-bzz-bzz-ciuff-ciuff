package api_trains.handlersGET.trainSolution.trainSolutionDecoder.time;

public interface Time {

  int getHours();

  int getMinutes();

  int getSeconds();
}
