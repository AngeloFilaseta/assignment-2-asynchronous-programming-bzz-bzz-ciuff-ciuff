package api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution;

import api_trains.handlersGET.trainSolution.trainSolutionDecoder.time.Time;

import java.util.List;

public interface Solution {
  String getID();

  List<String> getListOfTrainsName();

  String getDepartureStationName();

  String getArrivalStationName();

  Time getDepartureTime();

  Time getArrivalTime();

  Time getDuration();

  double getOriginalPrice();
}
