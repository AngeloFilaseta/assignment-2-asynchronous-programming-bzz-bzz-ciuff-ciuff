package api_trains.handlersGET.trainSolution;

import api_trains.handlersGET.HttpRequestStrategy;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.TrainSolutionParams;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.TrainsSolutionParamsImpl;
import io.vertx.ext.web.RoutingContext;

import java.net.URI;
import java.net.http.HttpRequest;

public class TrainSolutionHttpStrategy implements HttpRequestStrategy {

  private final RoutingContext routingContext;

  protected TrainSolutionHttpStrategy(final RoutingContext routingContext){
    this.routingContext = routingContext;
  }

  @Override
  public HttpRequest apply() {
    return HttpRequest
      .newBuilder()
      .GET()
      .uri(createURI())
      .build();
  }

  private URI createURI() throws NullPointerException, IllegalArgumentException{
    TrainSolutionParams params = new TrainsSolutionParamsImpl(routingContext.request());
    return URI.create(
      "https://www.lefrecce.it/msite/api/solutions?origin=" + params.getDeparture() +
        "&destination=" + params.getArrival() +
        "&arflag=A&adate="+ params.getDate() +
        "&atime=" + params.getHour() +
        "&adultno=1&childno=0&direction=A&frecce=false&onlyRegional=false"
    );
  }

}
