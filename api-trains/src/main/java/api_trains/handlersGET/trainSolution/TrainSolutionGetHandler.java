package api_trains.handlersGET.trainSolution;

import api_trains.handlersGET.GetHandler;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution.Solution;
import io.vertx.core.*;

import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class TrainSolutionGetHandler extends GetHandler<List<Solution>> {

  public static TrainSolutionGetHandler createInstance(final Vertx vertx, final RoutingContext routingContext){
    return new TrainSolutionGetHandler(vertx, routingContext);
  }

  private TrainSolutionGetHandler(final Vertx vertx, final RoutingContext routingContext){
    super(
      vertx,
      routingContext,
      new TrainSolutionHttpStrategy(routingContext),
      new TrainSolutionPromiseStrategy()
    );
  }

}
