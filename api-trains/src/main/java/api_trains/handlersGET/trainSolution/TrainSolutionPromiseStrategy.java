package api_trains.handlersGET.trainSolution;

import api_trains.handlersGET.CompletedPromiseValueStrategy;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution.Solution;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.solution.SolutionBuilder;
import api_trains.handlersGET.trainSolution.trainSolutionDecoder.time.TimeFactory;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class TrainSolutionPromiseStrategy implements CompletedPromiseValueStrategy<List<Solution>> {

  @Override
  public List<Solution> apply(final String response) {
    return parseSolutions(response);
  }

  private List<Solution> parseSolutions(final String solutionsToParse){
    JsonArray jsonSolutions = new JsonArray(solutionsToParse);
    List<Solution> solutions = new ArrayList<>();
    for(int i = 0; i < jsonSolutions.size(); i++){
      solutions.add(extractSolution(jsonSolutions.getJsonObject(i)));
    }
    return solutions;
  }

  private Solution extractSolution(final JsonObject jsonSolution){
    return new SolutionBuilder()
      .setID(jsonSolution.getString("idsolution"))
      .setListOfTrains(extractTrains(jsonSolution))
      .setDepartureStationName(jsonSolution.getString("origin"))
      .setArrivalStationName(jsonSolution.getString("destination"))
      .setDepartureTime(TimeFactory.fromLong(jsonSolution.getLong("departuretime")))
      .setArrivalTime(TimeFactory.fromLong(jsonSolution.getLong("arrivaltime")))
      .setDuration(TimeFactory.fromString(jsonSolution.getString("duration")))
      .setOriginalPrice(jsonSolution.getDouble("originalPrice"))
      .build();
  }

  private List<String> extractTrains(final JsonObject solution){
    JsonArray jsonArray = solution.getJsonArray("trainlist");
    List<String> result = new ArrayList<>();
    for(int i = 0; i < jsonArray.size(); i++){
      result.add(jsonArray.getJsonObject(i).getString("trainidentifier"));
    }
    return result;
  }

}
