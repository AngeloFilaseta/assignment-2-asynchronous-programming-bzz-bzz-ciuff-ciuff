import {Button, Form} from "react-bootstrap";
import FormEntry from "../../components/FormEntry";

export default function SolutionFormGroup(props) {
  return (
      <Form>
          <FormEntry type={"text"} handler={props.departureHandler} label={"Departure Station:"} placeholder={"Departure Station"}/>
          <FormEntry type={"text"} handler={props.arrivalHandler} label={"Arrival Station:"} placeholder={"Arrival Station"}/>
          <FormEntry type={"text"} handler={props.dateHandler} label={"Date:"} placeholder={"Date"}/>
          <FormEntry type={"text"} handler={props.timeHandler} label={"From Time:"} placeholder={"From Time"}/>
          <Button variant="primary" onClick={() => props.searchHandler()}>Search Solutions</Button>
      </Form>
    );
}
