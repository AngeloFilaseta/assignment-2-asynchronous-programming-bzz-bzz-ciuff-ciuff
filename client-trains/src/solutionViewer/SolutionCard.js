import {Card, Col, Container} from "react-bootstrap";
import {toTimeString} from "../util/TimeUtil";

export default function SolutionCard(props) {

    return (
        <Col>
            <Card>
                <Card.Header>{props.id}</Card.Header>
                <Card.Body>
                    <Card.Title>
                        {toTimeString(props.departureTime)} - {toTimeString(props.arrivalTime)}
                    </Card.Title>
                    <Card.Text>
                        <b>{props.departureStation} - {props.arrivalStation}</b>
                    </Card.Text>
                    <Card.Text>
                        <b>{props.originalPrice} EUR</b>
                    </Card.Text>
                    <Container>
                        <h4>Train List:</h4>
                        {updateList(props.trains, props.setMonitoredTrain)}
                    </Container>
                </Card.Body>
            </Card>
        </Col>
    );
}

function updateList(trains) {
    return (trains.map(train => <div key={train} className={"mx-1"}> {train} </div>));
}