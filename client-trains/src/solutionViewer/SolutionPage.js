import {Container, Row} from "react-bootstrap";
import SolutionCard from "./SolutionCard";
import SolutionFormGroup from "./form/SolutionFormGroup";
import {Component} from "react";
import $ from "jquery";
import {SERVER_URL} from "../util/Configurations";
import {ButtonHandler} from "../components/ButtonHandler";

export class SolutionPage extends Component{

    constructor(props) {
        super(props);
        this.state = {solutions: [], departureForm: "", arrivalForm: "", dateForm: "", timeForm: ""}
    }

    // Handlers used to update the values from all the forms
    handleDepartureStationForm = (event) => this.setState({departureForm: event.target.value});
    handleArrivalStationForm = (event) => this.setState({arrivalForm: event.target.value});
    handleDateForm = (event) => this.setState({dateForm: event.target.value});
    handleTimeForm = (event) => this.setState({timeForm: event.target.value});

    // this.state.solution is populated when the GET REQUEST is called.
    makeRequestAndUpdateInfo(){
        $.get(SERVER_URL + "/train-solution?departure="+ this.state.departureForm
                            +"&arrival=" + this.state.arrivalForm
                            +"&date=" + this.state.dateForm
                            +"&time=" + this.state.timeForm)
            .done((result) => {
                console.log("GET Request Done")
                this.setState({solutions: result});
            })
            .fail((result) => {
                console.log("An error occurred: " + result)
            })
    }

    render(){
        return (
            <>
                <ButtonHandler handler={() => this.props.goBackHandler()}
                                        text="Go Back"/>
                <SolutionFormGroup
                    searchHandler={() => this.makeRequestAndUpdateInfo()}
                    departureHandler = {this.handleDepartureStationForm}
                    arrivalHandler = {this.handleArrivalStationForm}
                    dateHandler = {this.handleDateForm}
                    timeHandler = {this.handleTimeForm}/>
                <Container fluid >
                    <h1>Solution Monitor</h1>
                    <Row>
                        {/* Load all the solutions found on the DOM.
                        check this.makeRequestAndUpdateInfo() method
                        to see the population of state.solutions. */}
                        {this.updateList(this.state.solutions)}
                    </Row>
                </Container>
            </>
        )
    }


    updateList(solutions) {
        return (
            <>
                {solutions.map(item => (
                    <SolutionCard
                        id={item.id}
                        trains={item.listOfTrainsName}
                        departureStation={item.departureStationName}
                        arrivalStation={item.arrivalStationName}
                        departureTime={item.departureTime}
                        arrivalTime={item.arrivalTime}
                        duration={item.duration}
                        originalPrice={item.originalPrice}
                        key={item.id}/>
                ))}
            </>
        );
    }
}

