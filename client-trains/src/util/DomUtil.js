import {ButtonHandler} from "../components/ButtonHandler";

export function placeGoBackButton(props){
    return <ButtonHandler handler={() => props.goBackHandler()}
                          text="Go Back"/>
}