import {Card, Col} from "react-bootstrap";
import {Component} from "react";

export class StationInfoEntry extends Component{

    render(){
        return (
            <Col>
                <Card>
                    <Card.Header>{this.props.name} </Card.Header>
                    <Card.Body>
                        <Card.Title>{this.props.station}</Card.Title>
                        <Card.Text>
                            Time: {this.props.time} <br/>
                            Scheduled Rail: {this.props.scheduledRail}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        )
    }

}
