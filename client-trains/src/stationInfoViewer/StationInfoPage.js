import {Container} from "react-bootstrap";
import {Component} from "react";
import $ from "jquery";
import {SERVER_URL} from "../util/Configurations";
import {ButtonHandler} from "../components/ButtonHandler";
import FormEntry from "../components/FormEntry";
import {StationMonitor} from "./StationMonitor";
import {placeGoBackButton} from "../util/DomUtil";

export class StationInfoPage extends Component{

    constructor(props) {
        super(props);
        this.state = {formStationCode:"", stationCode: "", stationInfo: {departures:[], arrivals:[]}}
    }

    // Update stationInfo with the result of the GET REQUEST
    makeRequestAndUpdateInfo(){
        this.setState({stationCode: this.state.formStationCode})
        $.get(SERVER_URL + "/real-time-station-info?stationCode=" + this.state.formStationCode)
            .done((result) => {
                console.log("GET Request Done")
                this.setState({stationInfo: result});
            })
            .fail((result) => {
                console.log("An error occurred: " + result)
            })
    }

    render(){
        if(this.state.stationCode === ""){ //This will be rendered if the button is not clicked
            return <Container fluid >
                {placeGoBackButton(this.props)}
                <FormEntry type={"text"}
                           handler={(event) => this.setState({formStationCode: event.target.value})}
                           label={"Station to monitor:"}
                           placeholder={"Station"}/>
                <ButtonHandler handler={() => this.makeRequestAndUpdateInfo()}
                               text="Monitor Station"/>
            </Container>
        } else {  //this will be rendered if the button is clicked
            return (
                <Container>
                    {placeGoBackButton(this.props)}
                    <h1>Monitoring: {this.props.id}</h1>
                    <StationMonitor stationInfo = {this.state.stationInfo}
                                    getRequest={()=> this.makeRequestAndUpdateInfo()}/>
                </Container>
            )
        }
    }


}
