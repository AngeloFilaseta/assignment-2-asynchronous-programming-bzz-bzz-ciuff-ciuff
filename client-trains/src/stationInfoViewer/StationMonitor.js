
import {Container, Row} from "react-bootstrap";
import {Component} from "react";
import {StationInfoEntry} from "./StationInfoEntry";
import {StartAndStopMonitor} from "../components/StartAndStopMonitor";
import {DELTA_REQUEST_TIME} from "../util/Configurations";

export class StationMonitor extends Component{

    componentDidMount() {
        this.startMonitoring()
    }

    componentWillUnmount() {
        this.stopMonitoring()
    }

    // Send a GET REQUEST to the server asking for the station status every DELTA_REQUEST_TIME
    startMonitoring(){
        console.log("monitoring started")
        this.timerID = setInterval(
            () => this.props.getRequest(),
            DELTA_REQUEST_TIME
        );
        this.props.getRequest()
    }

    // Stop sending requests (delete the timer)
    stopMonitoring(){
        console.log("monitoring stopped")
        clearInterval(this.timerID);
    }

    render(){
        return (
            <Container fluid >
                <h1>Departures</h1>
                <Row>
                    {/* Update the DOM with the Departures,
                    check upper component to see population of stationInfo */}
                    {this.updateList(this.props.stationInfo.departures)}
                </Row>
                <h1>Arrivals</h1>
                <Row>
                    {/* Update the DOM with the Arrivals
                    check upper component to see population of stationInfo */}
                    {this.updateList(this.props.stationInfo.arrivals)}
                </Row>
                <StartAndStopMonitor startHandler={() => this.startMonitoring()}
                                     stopHandler={() => this.stopMonitoring()}/>
            </Container>
        )
    }

    updateList(stops) {
        return (stops.map(item => (<StationInfoEntry name={item.name}
                                                     station={item.station}
                                                     time={item.time}
                                                     scheduledRail={item.scheduledRail}
                                                     key={item.name}/>)));
    }

}
