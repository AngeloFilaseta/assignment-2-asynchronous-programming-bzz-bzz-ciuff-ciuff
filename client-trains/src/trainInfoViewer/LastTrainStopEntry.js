import {Col} from "react-bootstrap";
import {UNKNOWN_MESSAGE} from "../util/Configurations";

export default function LastTrainStopEntry(props) {
    const lastStop = (props.stop === undefined) ? {station: UNKNOWN_MESSAGE,
                                                   scheduledTime: UNKNOWN_MESSAGE,
                                                   actualTime: UNKNOWN_MESSAGE} : props.stop
    return (
        <Col>
            <h4>Station name:</h4> <p>{lastStop.station}</p>
            <h4>Scheduled Time:</h4> <p>{lastStop.scheduledTime}</p>
            <h4>Actual Time:</h4> <p>{lastStop.actualTime === undefined ? UNKNOWN_MESSAGE : lastStop.actualTime}</p>
        </Col>
    )
}
