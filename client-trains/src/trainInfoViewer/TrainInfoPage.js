import {Container} from "react-bootstrap";
import {Component} from "react";
import {ButtonHandler} from "../components/ButtonHandler";
import FormEntry from "../components/FormEntry";
import {TrainMonitor} from "./TrainMonitor";
import $ from "jquery";
import {SERVER_URL} from "../util/Configurations";
import {placeGoBackButton} from "../util/DomUtil";

export default class TrainInfoPage extends Component{

    constructor(props) {
        super(props);
        this.state = {formTrainID:"", trainInfo: {name: "", departure:{}, arrival:{}, lastStop:{}}}
    }

    makeRequestAndUpdateInfo(){
        $.get(SERVER_URL + "/real-time-train-info?trainNumber=" + this.state.formTrainID)
            .done((result) => {
                console.log("GET Request Done")
                this.setState({trainInfo: result});
            })
            .fail((result) => {
                console.log("An error occurred: " + result);
            })
    }

    render(){
        if(this.state.trainInfo.name === ""){
            return (
                <Container fluid>
                    {placeGoBackButton(this.props)}
                    <FormEntry label={"Train ID:"}
                               type={"text"}
                               placeholder={"Train ID"}
                               handler = {(event) => this.setState({formTrainID: event.target.value})}/>
                    <ButtonHandler handler={() => this.makeRequestAndUpdateInfo()}
                                   text="Select Train"/>
                </Container>
            )
        } else {
            return(
                <>
                    {placeGoBackButton(this.props)}
                    <TrainMonitor name={this.state.trainInfo.name}
                                  departure={this.state.trainInfo.departure}
                                  arrival={this.state.trainInfo.arrival}
                                  lastStop={this.state.trainInfo.lastStop}
                                  getRequest={() => this.makeRequestAndUpdateInfo()}/>
                </>
            )
        }
    }

}
