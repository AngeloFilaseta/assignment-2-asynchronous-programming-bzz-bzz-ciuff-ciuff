import {Container, Row} from "react-bootstrap";
import TrainStopEntry from "./TrainStopEntry";
import {Component} from "react";
import LastTrainStopEntry from "./LastTrainStopEntry";
import {StartAndStopMonitor} from "../components/StartAndStopMonitor";
import {DELTA_REQUEST_TIME} from "../util/Configurations";

export class TrainMonitor extends Component{

    componentDidMount() {
        this.startMonitoring()
    }

    componentWillUnmount() {
        this.stopMonitoring()
    }

    // Send a GET REQUEST to the server asking for the train status every DELTA_REQUEST_TIME
    startMonitoring(){
        console.log("monitoring started")
        this.timerID = setInterval(
            () => this.props.getRequest(),
            DELTA_REQUEST_TIME
        );
        this.props.getRequest()
    }

    // Stop sending requests (delete the timer)
    stopMonitoring(){
        console.log("monitoring stopped")
        clearInterval(this.timerID);
    }

    render(){
        return(
            <Container>
                <h1>Monitoring: {this.props.name}</h1>
                <Row>
                    <TrainStopEntry stop={this.props.departure} />
                    <TrainStopEntry stop={this.props.arrival} />
                    <LastTrainStopEntry stop={this.props.lastStop} />
                </Row>
                {/* Start and stop monitoring handler as passed as props */}
                <StartAndStopMonitor startHandler={() => this.startMonitoring()}
                                     stopHandler={() => this.stopMonitoring()}/>
            </Container>
        )
    }
}



