import {Col} from "react-bootstrap";
import {UNKNOWN_MESSAGE} from "../util/Configurations";

export default function TrainStopEntry(props) {
    return (
        <Col>
            <h4>Station name:</h4> <p>{props.stop.station}</p>
            <h4>Scheduled Time:</h4> <p>{props.stop.scheduledTime}</p>
            <h4>Actual Time:</h4> <p>{props.stop.actualTime === undefined ? UNKNOWN_MESSAGE : props.stop.actualTime}</p>
            <h4>Scheduled Rail:</h4> <p>{props.stop.scheduledRail}</p>
            <h4>Actual Rail:</h4> <p>{props.stop.actualRail === undefined ? UNKNOWN_MESSAGE : props.stop.actualRail}</p>
        </Col>
    )
}