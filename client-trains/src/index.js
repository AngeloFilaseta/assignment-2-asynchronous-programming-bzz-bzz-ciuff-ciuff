import React from 'react';
import ReactDOM from 'react-dom';
import "bootstrap/dist/css/bootstrap.min.css"
import {HomePage} from "./HomePage";

ReactDOM.render(
  <React.StrictMode>
      <HomePage/>
  </React.StrictMode>,
  document.getElementById('root')
);
