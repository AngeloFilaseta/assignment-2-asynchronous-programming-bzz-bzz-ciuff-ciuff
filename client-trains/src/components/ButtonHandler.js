import {Button} from "react-bootstrap";
import {Component} from "react";

// Run the handler passed in props upon clicking
export class ButtonHandler extends Component {

    render(){
        return <Button variant={this.props.variant} onClick={() => this.props.handler()}>
                    {this.props.text}
               </Button>
    }

}
