import {Card} from "react-bootstrap";
import {Component} from "react";
import {ButtonHandler} from "./ButtonHandler";

//Used to start and stop the GET REQUESTS to the server, both handlers must be passed as props
export class StartAndStopMonitor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            monitoring: true
        };
    }

    render() {
        return (
            <Card>
                <Card.Header className={"text-center"}>
                    {this.state.monitoring ?
                        <h2 className={"text-success"}> Monitoring on going </h2> :
                        <h2 className={"text-danger"}> Monitoring is STOPPED </h2>}
                </Card.Header>
                <Card.Body className={"text-center"}>
                    <ButtonHandler variant={"success"}
                                   text={"Start Monitoring"}
                                   handler={() => {
                                       this.setState({ monitoring: true});
                                       this.props.startHandler()}}/>
                    <ButtonHandler variant={"danger"}
                                   text={"Stop Monitoring"}
                                   handler={() => {
                                       this.setState({monitoring: false});
                                       this.props.stopHandler()}}/>
                </Card.Body>
            </Card>
        );
    }
}
