import {Form} from "react-bootstrap";

// A Form Entry, an handler must be passed as props to get the value in the upper component
export default function FormEntry(props) {
    return (
        <Form.Group>
            <Form.Label>{props.label}</Form.Label>
            <Form.Control type={props.type}
                          placeholder={props.placeholder}
                          onChange={e => props.handler(e)}/>
        </Form.Group>
    );
}


