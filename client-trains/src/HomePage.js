import {Component} from "react";
import {SolutionPage} from "./solutionViewer/SolutionPage";
import TrainInfoPage from "./trainInfoViewer/TrainInfoPage";
import {StationInfoPage} from "./stationInfoViewer/StationInfoPage";
import {ButtonHandler} from "./components/ButtonHandler";

export class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {goToSolutionPage: false, goToTrainPage: false, goToStationPage: false}
    }

    goToSolutionPageHandler = () => this.setState({goToSolutionPage: true})
    goToTrainPageHandler = () => this.setState({goToTrainPage: true})
    goToStationPageHandler = () => this.setState({goToStationPage: true})
    goBackToHomepageHandler = () => this.setState({goToSolutionPage: false,
                                                         goToTrainPage: false,
                                                         goToStationPage: false})

    render() {
        if(this.state.goToSolutionPage === false &&
            this.state.goToTrainPage === false &&
            this.state.goToStationPage === false){
            return (<div>
                        <ButtonHandler handler={this.goToSolutionPageHandler}
                                       text={"Solution Page"}/>
                        <ButtonHandler handler={this.goToTrainPageHandler}
                                        text={"Train Info Page"}/>
                        <ButtonHandler handler={this.goToStationPageHandler}
                                       text={"Station Info Page"}/>
                    </div>);
        }else if(this.state.goToSolutionPage === true){
            return <SolutionPage goBackHandler={this.goBackToHomepageHandler}/>
        }else if(this.state.goToTrainPage === true){
            return <TrainInfoPage goBackHandler={this.goBackToHomepageHandler}/>
        }else if(this.state.goToStationPage === true){
            return <StationInfoPage goBackHandler={this.goBackToHomepageHandler}/>
        }
        return <h1 className={"text-danger"}>Error, something really wrong happened.</h1>
    }
}

